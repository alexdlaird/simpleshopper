from django.conf.urls import url, include

urlpatterns = [
    # TODO: We'll include Django's base authentication URLs here, which we would likely extend in the future
    url('^', include('django.contrib.auth.urls')),
]
