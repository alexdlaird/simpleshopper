from abc import abstractmethod

from simpleshopper.common.models import BaseModel


class BaseOwnedModel(BaseModel):
    """
    All models that inherit from this model are in some way related to a parent user who is associated with access
    permissions for the object. The reference to the user is not defined in this model, rather the model is abstract
    and requires that `get_user()` be implemented. This is a model not necessarily require a direct reference to the
    owning user, so adding that constraint would be limiting and unnecessary.
    """

    class Meta:
        abstract = True

    @abstractmethod
    def get_user(self):
        """
        Returns the user that owns this model. Note that since not all models necessarily have a direct reference to the
        user, so calling this function may indirectly query for user details.

        :return: The user that owns the implemented model.
        """
        raise NotImplementedError
