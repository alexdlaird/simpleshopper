from django.contrib.auth.models import AbstractUser
from simpleshopper.common.models import BaseModel


class User(AbstractUser, BaseModel):
    # TODO: Nothing to do here now, but we'd almost certainly want to abstract the user in the future, so defining the
    # user model here so it's something that, while relying on Django's base user model, can be easily extended

    def get_user(self):
        return self
