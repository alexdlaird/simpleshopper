from django.apps import AppConfig


class AuthConfig(AppConfig):
    name = 'simpleshopper.auth'
    label = 'simpleshopper_auth'
    verbose_name = "Authentication"
