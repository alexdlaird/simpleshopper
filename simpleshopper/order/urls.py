from django.conf.urls import url

from simpleshopper.order.views.cartproductviews import CartProductsListView, CartProductsDetailView
from simpleshopper.order.views.orderproductsviews import OrderProductsListView
from simpleshopper.order.views.orderviews import OrdersListView, OrdersDetailView

urlpatterns = [
    # API views
    url(r'cartproducts/$', CartProductsListView.as_view(), name='cartproducts-list'),
    url(r'cartproducts/(?P<pk>[0-9]+)/$', CartProductsDetailView.as_view(), name='cartproducts-detail'),

    url(r'orders/$', OrdersListView.as_view(), name='orders-list'),
    url(r'orders/(?P<pk>[0-9]+)/$', OrdersDetailView.as_view(), name='orders-detail'),

    url(r'orderitems/$', OrderProductsListView.as_view(), name='orderproducts-list'),
]
