from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import ListView
from django.views.generic.edit import UpdateView, DeleteView, CreateView

from simpleshopper.order.models import CartProduct


@method_decorator(login_required, name='dispatch')
class CartProductsListView(ListView, CreateView):
    """
    get:
    Return a list of all cart product instances for the currently authenticated user.

    post:
    Create a new cart product instance for the authenticated user and given product.
    """
    fields = ['quantity', 'category', 'product_id']

    def get_queryset(self):
        user = self.request.user
        return CartProduct.objects.filter(user_id=user.pk)

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(CartProductsListView, self).form_valid(form)


@method_decorator(login_required, name='dispatch')
class CartProductsDetailView(UpdateView, DeleteView):
    """
    put:
    Update the quantity for the given cart product instance.

    delete:
    Delete the given cart product instance.
    """
    model = CartProduct
    fields = ['quantity']

    def get_queryset(self):
        user = self.request.user
        return CartProduct.objects.filter(user_id=user.pk)
