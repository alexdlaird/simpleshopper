import uuid

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView

from simpleshopper.order.models import Order, CartProduct, OrderProduct


@method_decorator(login_required, name='dispatch')
class OrdersListView(ListView, CreateView):
    """
    get:
    Return a list of all order instances for the currently authenticated user.

    post:
    Create a new order instance for the authenticated user based on what is currently in their cart.
    """

    def get_queryset(self):
        user = self.request.user
        return Order.objects.filter(user_id=user.pk)

    def form_valid(self, form):
        user = self.request.user

        form.instance.user = user
        # TODO: in a highly scalable environment, we would want to hand off ID generation to some ID creation service
        # to guarantee uniqueness across nodes
        form.instance.order_id = uuid.uuid4()

        response = super(OrdersListView, self).form_valid(form)

        # Once an order is created, create order product instances from what is currently in the user's cart, then
        # clear the cart; to make this step more robust, we would want to take concurrency issues into account
        cart_products = CartProduct.objects.filter(user_id=user.pk)
        for cart_product in cart_products:
            # TODO: When a Manager is created for an order product, implementing a custom `create` method that takes a
            # a cart product and order would improve the abstraction here, and it would also be how we would get the
            # related product model to determine the price at time of purchase
            OrderProduct.objects.create(price=cart_product.price,
                                        quantity=cart_product.quantity,
                                        category=cart_product.category,
                                        product_id=cart_product.product_id,
                                        order=form.instance)

        cart_products.delete()

        return response


@method_decorator(login_required, name='dispatch')
class OrdersDetailView(DetailView, UpdateView):
    """
    get:
    Return the given order instance for the authenticated user.

    put:
    Update the status for the given order instance.
    """
    fields = ['status']

    def get_queryset(self):
        user = self.request.user
        return Order.objects.filter(user_id=user.pk)
