from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import ListView

from simpleshopper.order.models import OrderProduct


@method_decorator(login_required, name='dispatch')
class OrderProductsListView(ListView):
    """
    get:
    Return a list of all order product instances for the currently authenticated user. Optionally, an `order` parameter
    can be passed in via the URL to only retrieve the list of order product instances for a given order.
    """

    def get_queryset(self):
        user = self.request.user
        queryset = OrderProduct.objects.filter(order__user_id=user.pk)
        if 'order' in self.request.GET:
            queryset = queryset.filter(order_id=self.request.GET.get('order'))
        return queryset
