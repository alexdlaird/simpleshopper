from django.conf import settings
from django.db import models

from simpleshopper.auth.models import BaseOwnedModel


class Order(BaseOwnedModel):
    """
    This model would be instantiated at the point a user's cart is processed.
    """

    PENDING = 0
    CANCELLED = 1
    SHIPPED = 2
    RETURNED = 3
    STATUS_CHOICES = (
        (PENDING, 'Pending'),
        (CANCELLED, 'Cancelled'),
        (SHIPPED, 'Shipped'),
        (RETURNED, 'Returned'),
    )

    order_id = models.CharField(max_length=255)

    status = models.PositiveIntegerField(choices=STATUS_CHOICES, default=PENDING)

    # TODO: Here our retention policy would come into play. Should we CASCADE delete, or should we set to NULL and
    # retain anonymized order information after a user has deleted their account? Likely the latter, but would be
    # defined by requirements and outside of the scope of this implementation.
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='orders', on_delete=models.CASCADE)

    def __str__(self):
        return '#{} ({})'.format(self.order_id, self.get_user().get_username())

    def get_user(self):
        return self.user
