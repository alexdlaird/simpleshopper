from django.conf import settings
from django.db import models

from simpleshopper.auth.models import BaseOwnedModel
from simpleshopper.shop.models import BaseProductModel


class CartProduct(BaseOwnedModel):
    """
    The product relationship can be looked up using the `category` and `product_id` to identify the table and object
    references.
    """

    quantity = models.PositiveIntegerField()

    category = models.PositiveIntegerField(choices=BaseProductModel.CATEGORY_CHOICES)

    product_id = models.PositiveIntegerField()

    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='cart_products', on_delete=models.CASCADE)

    # TODO: Here we would define a custom manager for this model that provides useful helper methods to elegantly make
    # the queries above based on `category` and `product_id`
    # objects = CartProductManager()

    def get_user(self):
        return self.user
