from django.db import models

from simpleshopper.auth.models import BaseOwnedModel
from simpleshopper.shop.models import BaseProductModel


class OrderProduct(BaseOwnedModel):
    """
    This model would be instantiated from a clone of each item in a user's cart on purchase.

    The model first contains a reference to its parent product. For fields that may change but need to be stored for
    reference at the time of order (for instance, `price`), fields should also be explicitely defined in this model.

    The product relationship can be looked up using the `category` and `product_id` to identify the table and object
    references.
    """

    price = models.DecimalField(max_digits=10, decimal_places=2)

    quantity = models.PositiveIntegerField()

    category = models.PositiveIntegerField(choices=BaseProductModel.CATEGORY_CHOICES)

    product_id = models.PositiveIntegerField()

    # TODO: Here our retention policy would come into play. Should we CASCADE delete, or should we set to NULL and
    # retain anonymized order information after a user has deleted their account? Likely the latter, but would be
    # defined by requirements and outside of the scope of this implementation.
    order = models.ForeignKey('Order', related_name='order_products', on_delete=models.CASCADE)

    # TODO: Here we would define a custom manager for this model that provides useful helper methods to elegantly make
    # the queries above based on `category` and `product_id`
    # objects = OrderProductManager()

    def get_user(self):
        return self.order.get_user()
