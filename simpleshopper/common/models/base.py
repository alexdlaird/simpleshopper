from django.db import models


class BaseModel(models.Model):
    """
    All other database models should inherit from this model to ensure consistency of base attributes across all models.
    """

    created_at = models.DateTimeField(auto_now_add=True, db_index=True)

    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True
