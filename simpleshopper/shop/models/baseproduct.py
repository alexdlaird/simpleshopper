from abc import abstractmethod

from simpleshopper.common.models import BaseModel
from django.db import models


class BaseProductModel(BaseModel):
    """
    All shop `Product` database models should inherit from this model, as it contains fields common to all shop items.
    """

    CLOTHING = 0
    CATEGORY_CHOICES = (
        (CLOTHING, 'Clothing'),
    )

    name = models.CharField(db_index=True)

    catalog_number = models.CharField(max_length=30, db_index=True)

    price = models.DecimalField(max_digits=10, decimal_places=2)

    description = models.TextField()

    class Meta:
        abstract = True

    @abstractmethod
    def get_category(self):
        """
        Returns the category string of the subclass.
        """
        raise NotImplementedError

    def __str__(self):
        return '#{} ({} in {})'.format(self.catalog_number, self.name, self.get_category())
