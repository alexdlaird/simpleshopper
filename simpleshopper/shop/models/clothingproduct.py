from django.db import models

from simpleshopper.shop.models import BaseProductModel


class ClothingProduct(BaseProductModel):
    """
    Providing one `Product` type here as a barebones example. In this proposed implementation, each `Product` type have
    its own table, and reference lookups (for instance, see `OrderItem` in the `order` app) would be done by determing
    the table using `category` and then looking up by the the product's `id`.
    """

    SHIRT = 0
    PANTS = 1
    HAT = 2
    CLOTHING_CHOICES = (
        (SHIRT, 'Shirt'),
        (PANTS, 'Pants'),
        (HAT, 'Hat'),
    )

    type = models.PositiveIntegerField(choices=CLOTHING_CHOICES)

    size = models.PositiveIntegerField()

    color = models.CharField()

    def get_category(self):
        return BaseProductModel.CLOTHING
