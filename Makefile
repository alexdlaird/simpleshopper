.PHONY: all install build migrate test

all: install build migrate test

install: env
	python -m pip install -r requirements.txt --user

build:
	python manage.py collectstatic --noinput

migrate:
	python manage.py migrate

test:
	python manage.py test
