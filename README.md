# Code Challenge Prompt

1. Describe a general architecture for a distributed, scalable system that would
serve as a backend for a hypothetical webstore and shopping cart type application.
The requirements would be that individual users would have records of what was
purchased and their properties, and be able to return to an existing cart of items
if they had a cart that wasn't emptied between sessions by check-out.

2. Implement in code using a bare-bones version of this shopping backend cart and
the purchase history component as a 'Proof of Concept. It must understand that
different products have certain fixed properties (name, price), and many specific
properties (ie, t-shirt sizes, color of hat, flavor of candy, etc).

The code should be modular enough to easily add new products or integrate with a
hypothetical frontend. It should also have the ability for a specified user to
return to an existing cart of items, then update the user's purchase history and
emptying the cart on check-out.

Please specify your reasoning behind why you implemented this PoC in a certain
way and if it differed from the earlier description, and what choices were made
when deciding what to implement and what to stub out.

# Response

## Scalable Architecture

Broadly speaking, at a bare minimum for a horizontally scalable web services that
can serve a simple webstore, we would want to ensure our implementation were
stateless. Initial considerations would be a distributed datastore for long-lived,
relational data, likely a MySQL database. Use of a distributed cache would also be
necessary for authentication, session, and other more short-lived storage. Given
these considerations, a simple webstore could then scale horizontally behind an
ELB, which would also allow us to minimize (if not eliminate, depending on how
deployments themselves were done) downtime during releases windows.

Given the relatively small constraints of a webstore, NoSQL or other schemaless
datastores would likely not be necessary. Unless our initial requirements strongly
implied heavy post processing of user and order information, a relational datastore
should be sufficient for both the near and long term.

## Models

For the problem description above, we might consider the following models:

* User
    * A simple user model necessary for authentication as well as cart and order management
* ClothingProduct
    * Each category of product would have its own model that defines category-specific fields
* CartProduct
    * An item in the user's cart, relating to the product that it references
* Order
    * An order that is created when the user's cart is processed
* OrderProduct
    * When an order is processed, all item's in the user's cart are recreated as order product's,
    saving their necessary states at that moment in time

## Scalable Implementation Decisions

We first want to define our app domain modules to isolate app-specific code. For a simple shop, we
will consider the following apps:

* conf
    * Boot and configuration management
* common
* auth
    * Models and service implementations specific to user authentication
* shop
    * Models and service implementations for shop products (should not be aware of any other app
    except `common` and `conf`)
* order
    * Models and service implementations for user order management, implementing the relationship
    between `auth` and `shop`

Breaking code into its associated domains ensures our code is modular and self-contained. In the future,
we would want to take this one step further and turn each app into it's own deployable package with
defined dependencies, which would in turn also allow us to deploy apps in a more well-defined manner. In
general, this keeps our code clean while also providing reusable and extensible modules for the future.

### Settings

To make deployment easier, we would want to abstract `settings.py` out into different configuration module.
For example, we would likely create a `configs` module in `conf` and define a `test.py`, `dev.py`, and
`deploy.py`, where `deploy.py` defines a prod-like deployment configuration. `deploy.py` would rely on
environment variables so it would still be environment-agnostic. All of the configuration module might first
read from `common.py`. `settings.py` would then be refactored to read `common.py`, then determine the
environment in which it is booting and read the appropriate configuration module.

### Better REST

The basic views implemented in this will utilize simple form-based actions. For a robust REST service, a
simple mixin should be implemented to abstract requests and responses to support JSON bodies.

### Future Authentication

For simplicity, this implementation will utilize simple session-based authentication. However,
when building a true REST backend, other authentication mechanisms would also want to be defined,
most likely relying primarily on some token based authentication system. OAuth is a likely candidate.